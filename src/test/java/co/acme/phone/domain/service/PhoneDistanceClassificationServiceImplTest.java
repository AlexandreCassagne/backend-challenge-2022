package co.acme.phone.domain.service;

import co.acme.phone.domain.CallType;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PhoneDistanceClassificationServiceImplTest {

    private final PhoneDistanceClassificationServiceImpl service = new PhoneDistanceClassificationServiceImpl();
    private final String na_1 =  "+11234567890";
    private final String na_2 =  "+13891930312";
    private final String na_3 =  "+13891930313";
    private final String uk   = "+447938592483";

    @Test
    void testAreaCode() {
        assertThat(service.areaCode(service.parsePhoneNumber(na_1))).isEqualTo("123");
        assertThat(service.areaCode(service.parsePhoneNumber(na_2))).isEqualTo("389");
        assertThat(service.areaCode(service.parsePhoneNumber(na_3))).isEqualTo("389");
        assertThat(service.areaCode(service.parsePhoneNumber(uk))).isEqualTo("793");
    }

    @Test
    void testPhoneNumberClassification() {
        assertThat(service.classify(na_1, na_2)).isEqualTo(CallType.long_distance);
        assertThat(service.classify(na_1, na_3)).isEqualTo(CallType.long_distance);
        assertThat(service.classify(uk, na_3)).isEqualTo(CallType.international);
        assertThat(service.classify(na_1, uk)).isEqualTo(CallType.international);
        assertThat(service.classify(na_2, na_3)).isEqualTo(CallType.local);
    }
}