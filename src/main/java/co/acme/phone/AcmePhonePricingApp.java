package co.acme.phone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcmePhonePricingApp {

    public static void main(String[] args) {
        SpringApplication.run(AcmePhonePricingApp.class, args);
    }

}
