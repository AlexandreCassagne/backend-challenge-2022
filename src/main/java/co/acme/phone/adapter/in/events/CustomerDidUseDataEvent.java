package co.acme.phone.adapter.in.events;

import lombok.Data;

@Data
public class CustomerDidUseDataEvent {
    private String customerId;

    /**
     * in bytes
     */
    private Long usage;
    
    private Boolean exempt;
}
