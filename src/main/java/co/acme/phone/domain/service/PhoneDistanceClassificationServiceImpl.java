package co.acme.phone.domain.service;

import co.acme.phone.domain.CallType;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class PhoneDistanceClassificationServiceImpl implements PhoneDistanceClassificationService {

    private final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    @Override
    public CallType classify(String originator, String receivingParty) {
        PhoneNumber originatorNumber = parsePhoneNumber(originator);
        PhoneNumber receivingPartyNumber = parsePhoneNumber(receivingParty);

        // Phone numbers are international if their country codes differ
        if (originatorNumber.getCountryCode() != receivingPartyNumber.getCountryCode()) {
            return CallType.international;
        }
        // the following rule is rather simple but probably much more complicated in real life
        // e.g. just in North America: https://en.wikipedia.org/wiki/List_of_North_American_Numbering_Plan_area_codes
        // but we'll keep it simple: if two phone numbers in the same country have matching first-three-characters,
        // then we say they are local to one another.

        else if (areaCode(originatorNumber).equals(areaCode(receivingPartyNumber))) {
            return CallType.local;
        }
        // Otherwise, they are long distance.
        else {
            return CallType.long_distance;
        }
    }

    /**
     * extract the first 3 characters of a phone number
     */
    String areaCode(PhoneNumber phoneNumber) {
        long nationalNumber = phoneNumber.getNationalNumber();
        return String.format("%d", nationalNumber).substring(0, 3);
    }

    PhoneNumber parsePhoneNumber(String phoneNumber) {
        try {
            return phoneNumberUtil.parse(phoneNumber, null);
        } catch (NumberParseException e) {
            throw new IllegalArgumentException("Invalid phone number " + phoneNumber + ". " +
                    "Phone numbers must start with the '+' character followed by the phone number, with no spaces.");
        }
    }
}
