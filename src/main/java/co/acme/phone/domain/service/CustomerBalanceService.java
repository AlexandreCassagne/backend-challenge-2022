package co.acme.phone.domain.service;

import co.acme.phone.domain.BillItem;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;


@Validated
public interface CustomerBalanceService {
    void consumeBillItem(@Valid BillItem billItem);

    Long amountDue(@NotEmpty String phoneNumber);

    void receivePayment(@NotEmpty String phoneNumber, @Positive Long amount);
}