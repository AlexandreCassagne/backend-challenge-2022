package co.acme.phone.domain;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;
import java.time.Instant;

@Data
public class BillItem {
    /**
     * the customer's phone number
     */
    @NotEmpty
    private String customerId;

    /**
     * the amount, in thousandths of dollars (1/10 cents)
     */
    @PositiveOrZero
    private Long amount;

    /**
     * the date this bill item corresponds to
     */
    private Instant instant;
}
